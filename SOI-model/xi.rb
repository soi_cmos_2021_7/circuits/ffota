def xi a1ColNum # like 'AZ' to index conversion
  base10Num = 0;
  (0..a1ColNum.length-1).each{|i|
    base10Num = base10Num + (a1ColNum[a1ColNum.length-1-i].ord - ('A'.ord) +1) * (26**i)
  }
  base10Num;
end
